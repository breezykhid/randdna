#include <string>
#include <random>
#include <iostream>

using namespace std;
using std::string;

string randDNA(int s, string b, int n)
{
	
	std::mt19937 r1(s);
	
	string bas = "";
	int value;
	//int max;
	int min = 0;
	int max = b.size()-1;
	
	std::uniform_int_distribution<> unifrm (0,max);
	
	for (int l=0; l<n; l++)
	{
		value = unifrm(r1);
		bas = bas + b[value];
	}

return bas;
}
